package com.example.ece_tp1;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//activité d'édition de taux
public class EditActivity extends AppCompatActivity {

    private Spinner spinnerEdit;
    private HashMap<String,Float> current_values;
    private EditText textValue;
    private Button bouton;
    private SQLdb database;
    private String keySelected;

    //Instanciation des listener pour réaliser les modifications des taux
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);


        Intent myIntent = getIntent();
        current_values = (HashMap<String, Float>) myIntent.getSerializableExtra("mymap");


        spinnerEdit = findViewById(R.id.spinner_edit);
        textValue = findViewById(R.id.textValue);
        bouton = findViewById(R.id.button_edit);
        database = new SQLdb(this);

        List<String> spinnerArray =  new ArrayList<String>();
        for(Map.Entry<String, Float> entry : current_values.entrySet()) {
            String key = entry.getKey();
            spinnerArray.add(key);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEdit.setAdapter(adapter);
        Log.d("INFO", "Spinner Edit MAJ");

        spinnerEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                textValue.setHint(current_values.get(spinnerEdit.getSelectedItem().toString()).toString());
                keySelected = spinnerEdit.getSelectedItem().toString();
                Log.d("INFO", "Item selectionné et affichage du taux actuel");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bouton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                Float value = Float.parseFloat(textValue.getText().toString());
                current_values.replace(keySelected,value);
                setResult(Activity.RESULT_OK,
                        new Intent().putExtra("map", current_values));
                Log.d("INFO", "Donnée MAJ renvoyé");
                finish();
                }


        });







    }
}
