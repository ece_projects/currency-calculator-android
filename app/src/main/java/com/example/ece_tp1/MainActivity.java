package com.example.ece_tp1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

//Classe de l'activité principale
public class MainActivity extends AppCompatActivity {

    private static double e_dollar = 1.16, e_yen = 130.54, d_yen = 112.95;
    private static String symb_dollar = "$", symb_euro = "€", symb_yen = "¥";
    private HashMap<String, Float> current_values;
    private HashMap<String, String> list_symb;

    private Button myButton,backButton;
    private Spinner mySpinner1, mySpinner2;
    private EditText number1, number2;
    private AlertDialog.Builder builder;
    private boolean isOffline;
    private SQLdb database;
    private Menu menu;
    private TextView symb1,symb2;


    //Fonction appelée a la création de l'activité
    //Instanciation de tous les listener
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("INFO", "lancement de l'application" + getString(R.string.app_name));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        myButton = findViewById(R.id.button_convert);
        mySpinner1 = findViewById(R.id.spinner_choix1);
        mySpinner2 = findViewById(R.id.spinner_choix2);
        number1 = findViewById(R.id.value_field1);
        number2 = findViewById(R.id.value_field2);
        symb1 = findViewById(R.id.text_symb1);
        symb2 = findViewById(R.id.text_symb2);
        number2.setEnabled(false);



        database = new SQLdb(this);

        list_symb = parseSymb();
        Log.d("INFO", "Symboles chargés");

        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setPositiveButton(R.string.dialog_positiv, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getCurrentValues();
                if(current_values==null){
                    displayError();
                }else{
                    isOffline = false;
                    setValuesForSpinner();
                    dispMessage(getString(R.string.dialog_positiv_mess));

                }

            }
        }).setNegativeButton(R.string.dialog_negativ, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                current_values=database.getAllPosts();
                setValuesForSpinner();

                MenuItem item = menu.findItem(R.id.menu_new_content_networkMode);
                item.setTitle(R.string.online_title);

                isOffline = true;

                dispMessage(getString(R.string.online_disp));
                dialog.cancel();
            }
        });

        getCurrentValues();
        if(current_values==null){
            displayError();
        }else{
            isOffline = false;
            setDatabase();
            setValuesForSpinner();
        }


        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if (number1.getText().toString().equals("0")) {
                    number1.setError(getString(R.string.value_nul_error));
                    Log.d("INFO", "Valeur Nulle");
                } else if (mySpinner1.getSelectedItem().toString().equals(mySpinner2.getSelectedItem().toString())) {
                    number1.setError(getString(R.string.value_same_curr));
                    Log.d("INFO", "Même devise selectionnée");
                } else if (number1.getText().length() == 0) {
                    number1.setError(getString(R.string.value_missing));
                    Log.d("INFO", "Pas de valeur entrée");
                } else {
                    float value = Integer.parseInt(number1.getText().toString());
                        number2.setText(Float.toString(((value/current_values.get(mySpinner1.getSelectedItem().toString())) * current_values.get(mySpinner2.getSelectedItem().toString()))));
                        Log.d("INFO", "Calcul de devise effectué");
                }


            }
        });

        mySpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(list_symb.containsKey(mySpinner1.getSelectedItem().toString())){

                    symb1.setText(list_symb.get(mySpinner1.getSelectedItem().toString()));
                    Log.d("INFO", "Symbole MAJ");
                }else{
                    symb1.setText(getString(R.string.cur_symb_missing));
                    Log.d("INFO", "Symbole manquant");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mySpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(list_symb.containsKey(mySpinner2.getSelectedItem().toString())){

                    symb2.setText(list_symb.get(mySpinner2.getSelectedItem().toString()));
                    Log.d("INFO", "Symbole MAJ");
                }else{
                    symb2.setText(getString(R.string.cur_symb_missing));
                    Log.d("INFO", "Symbole manquant");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });






    }


    //Transforme la hashmap en adapter pour l'affichage dans le spinner
    private ArrayAdapter<String> get_cur_list(HashMap<String, Float> hash){
        List<String> spinnerArray =  new ArrayList<String>();
        for(Map.Entry<String, Float> entry : hash.entrySet()) {
            String key = entry.getKey();
            spinnerArray.add(key);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    //création du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        this.menu = menu;
        Log.d("INFO", "Menu de l'activité crée");
        return true;
    }

    //Action sur selection item du menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_new_content_settings){
            setSettingsActivity();


        }else if(id == R.id.menu_new_content_refresh){
            if(!isOffline){
                getCurrentValues();
                if(current_values==null){
                    displayError();
                }else{
                    isOffline = false;
                    setValuesForSpinner();
                    dispMessage(getString(R.string.dialog_positiv_mess));
                }
            }else{
                current_values=database.getAllPosts();
                setValuesForSpinner();
                dispMessage(getString(R.string.dialog_positiv_mess));

            }


        }else if(id == R.id.menu_new_content_networkMode){
            if(isOffline){
                getCurrentValues();
                if(current_values==null) {
                    displayError();
                }else{
                    item.setTitle(getString(R.string.dialog_negativ));
                    isOffline=false;
                    dispMessage(getString(R.string.online_disp));
                }
            }else{
                current_values= database.getAllPosts();
                Log.d("INFO", "Data récupérée de la Database");
                item.setTitle(R.string.online_title);
                isOffline=true;
                dispMessage(getString(R.string.dialog_negativ_mess));
            }
        }else if(id == R.id.menu_new_content_edit){
            if(isOffline){
                setEditActivity();
            }else {
                dispMessage(getString(R.string.edition_mode_error));
            }

        }
        return super.onOptionsItemSelected(item);
    }

    //Création et lancement de l'activité settings
    public void setSettingsActivity(){
        Intent intent = new Intent(this, SettingsActivity.class);
        ArrayList<String> spinnerArray =  new ArrayList<String>();
        for(Map.Entry<String, Float> entry : current_values.entrySet()) {
            String key = entry.getKey();
            //Float value = entry.getValue();
            String value = entry.getValue().toString();
            spinnerArray.add(key + "   |    "+value);

            // do what you have to do here
            // In your case, another loop.
        }
        intent.putStringArrayListExtra("myList",spinnerArray);
        Log.d("INFO", "Lancement de l'activité settings");
        startActivity(intent);
    }

    //Création et lancement de l'activité edit
    public void setEditActivity(){
        Intent intent = new Intent(this,EditActivity.class);
        intent.putExtra("mymap",current_values);
        Log.d("INFO", "Lancement de l'activité edit");
        startActivityForResult(intent,0);
    }


    //Récuperation des données en ligne via asynctask
    private void getCurrentValues(){
        try {
            current_values = (HashMap<String, Float>) new RetrieveFeedTask().execute().get();
            Log.d("INFO", "Data récupérées Online");
        } catch (ExecutionException | InterruptedException e) {
            Log.d("INFO", "Echec récupération Online");
        }
    }

    //Affichage de l'alert dialog d'erreur
    private void displayError(){
        AlertDialog alert = builder.create();
        alert.setTitle(getString(R.string.data_req_title));
        alert.setMessage(getString(R.string.data_req_mess));
        Log.d("INFO", "Dialog alerte crée et affichée");
        alert.show();
    }

    //Met les entries dans les spinner
    private void setValuesForSpinner(){
        mySpinner1.setAdapter(this.get_cur_list(current_values));
        mySpinner2.setAdapter(this.get_cur_list(current_values));
        Log.d("INFO", "MAJ des entrées des spinners");
    }

    //Création et modification de la database
    private void setDatabase(){

        for(Map.Entry<String, Float> entry : current_values.entrySet()) {

            database.addOrUpdateUser(entry.getKey(), entry.getValue().toString());

            // do what you have to do here
            // In your case, another loop.
        }
        Log.d("INFO", "MAJ données Database: ");
    }

    //Récupération de résultat de l'activité édit
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            current_values = (HashMap<String, Float>) data.getSerializableExtra("map");
            setDatabase();
            dispMessage(getString(R.string.dialog_positiv_mess));
        }
    }

    //Affichage d'un message avec toast
    private void dispMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Log.d("INFO", "Message affiché : " + message);
    }

    //Parse le string d'array de symboles
    private HashMap<String,String> parseSymb(){
        HashMap<String,String> map = new HashMap<String,String>();

        Resources res = getResources();
        TypedArray list_cur = res.obtainTypedArray(R.array.curr);
        TypedArray list_symb = res.obtainTypedArray(R.array.symb);

        for(int i = 0; i<list_cur.length(); i++){
            map.put(list_cur.getString(i),list_symb.getString(i));
        }

        Log.d("INFO", "Hashmap symb crée");
        return map;

    }



    }
