package com.example.ece_tp1;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//Classe controle de la database
public class SQLdb extends SQLiteOpenHelper {
    // Database Info
    private static final String DATABASE_NAME = "tp5rates";
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE_CURRENCYS = "currencys";

    // Post Table Columns
    private static final String KEY_CURRENCYS_ID = "id";
    private static final String KEY_CURRENCYS_CURRENCY_NAME = "currencyName";
    private static final String KEY_CURRENCYS_RATE = "rate";


    public SQLdb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
    }

    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CURRENCY_TABLE = "CREATE TABLE " + TABLE_CURRENCYS +
                "(" +
                KEY_CURRENCYS_ID + " INTEGER PRIMARY KEY," +
                KEY_CURRENCYS_CURRENCY_NAME + " TEXT," +
                KEY_CURRENCYS_RATE + " TEXT" +
                ")";

        db.execSQL(CREATE_CURRENCY_TABLE);
    }

    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_CURRENCYS);
            onCreate(db);
        }
    }

    //Suppresion de toutes les entrées de la database
    public void deleteAllCurrency() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            // Order of deletions is important when foreign key relationships exist.
            db.delete(TABLE_CURRENCYS, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }

    //Récupération des entrées de la database dans une hashmap
    @SuppressLint("Range")
    public HashMap<String, Float> getAllPosts() {
        HashMap<String, Float> dbvalues = new HashMap<String, Float>();

        // SELECT * FROM POSTS
        // LEFT OUTER JOIN USERS
        // ON POSTS.KEY_POST_USER_ID_FK = USERS.KEY_USER_ID
        String POSTS_SELECT_QUERY =
                String.format("SELECT * FROM %s",
                        TABLE_CURRENCYS);

        // "getReadableDatabase()" and "getWriteableDatabase()" return the same object (except under low
        // disk space scenarios)
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {

                    dbvalues.put(cursor.getString(cursor.getColumnIndex(KEY_CURRENCYS_CURRENCY_NAME)),Float.parseFloat(cursor.getString(cursor.getColumnIndex(KEY_CURRENCYS_RATE))));
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return dbvalues;
    }

    // Insert a post into the database
    public void addPost(String name, String rate) {
        // Create and/or open the database for writing
        SQLiteDatabase db = getWritableDatabase();

        // It's a good idea to wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
        db.beginTransaction();
        try {
            // The user might already exist in the database (i.e. the same user created multiple posts).
            ContentValues values = new ContentValues();
            values.put(KEY_CURRENCYS_CURRENCY_NAME, name);
            values.put(KEY_CURRENCYS_RATE, rate);
            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(TABLE_CURRENCYS, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    //insert or update entrée dans la base
    public long addOrUpdateUser(String name, String rate) {
        // The database connection is cached so it's not expensive to call getWriteableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        long userId = -1;

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_CURRENCYS_CURRENCY_NAME, name);
            values.put(KEY_CURRENCYS_RATE, rate);

            // First try to update the user in case the user already exists in the database
            // This assumes userNames are unique
            int rows = db.update(TABLE_CURRENCYS, values, KEY_CURRENCYS_CURRENCY_NAME + "= ?", new String[]{name});

            // Check if update succeeded
            if (rows == 1) {
                // Get the primary key of the user we just updated
                String usersSelectQuery = String.format("SELECT %s FROM %s WHERE %s = ?",
                        KEY_CURRENCYS_ID, TABLE_CURRENCYS, KEY_CURRENCYS_CURRENCY_NAME);
                Cursor cursor = db.rawQuery(usersSelectQuery, new String[]{String.valueOf(name)});
                try {
                    if (cursor.moveToFirst()) {
                        userId = cursor.getInt(0);
                        db.setTransactionSuccessful();
                    }
                } finally {
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                    }
                }
            } else {
                // user with this userName did not already exist, so insert new user
                userId = db.insertOrThrow(TABLE_CURRENCYS, null, values);
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add or update user");
        } finally {
            db.endTransaction();
        }
        return userId;
    }


}