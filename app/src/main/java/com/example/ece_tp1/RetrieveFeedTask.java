package com.example.ece_tp1;

import android.os.AsyncTask;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.net.URL;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

//Classe récuparation données online avec asynctask
class RetrieveFeedTask extends AsyncTask<URL, Void, HashMap<String, Float>> {

    private Exception exception;

    //Récupère le XML et le parse dans une hashmap
    protected HashMap<String, Float> doInBackground(URL... urls) {
        try {
            HashMap<String, Float> current_values = new HashMap<String, Float>();

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            db = dbf.newDocumentBuilder();
            Document doc = db.parse(new URL("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml").openStream());
            Log.d("INFO", "requete réseau XML récupéré");

            NodeList rootElement = doc.getChildNodes();
            NodeList sortByNodes = rootElement.item(0).getChildNodes();

                Node sortBy = sortByNodes.item(5);



                    NodeList letterRows = sortBy.getChildNodes();
                    Node letterRow = letterRows.item(1);
                    NodeList letterRowDetails = letterRow.getChildNodes();

                    String letter = null;
                    String guess = null;
                    NamedNodeMap detail;
                    for (int i = 1; i < letterRowDetails.getLength(); i+=2) {
                         detail = letterRowDetails.item(i).getAttributes();
                        letter = detail.getNamedItem("currency").getTextContent();
                        guess = detail.getNamedItem("rate").getTextContent();

                        current_values.put(letter,Float.parseFloat(guess));

                        Log.d("INFO", "currency parsed => currency=" + letter + ", rate="+guess);
                    }
                    current_values.put("EUR", 1F);
                    return current_values;

            //}






        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }

    //fonction qui va retourner la hashmap
    protected void onPostExecute(HashMap<String, Float> feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }


}

