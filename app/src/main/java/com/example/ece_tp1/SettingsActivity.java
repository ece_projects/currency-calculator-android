package com.example.ece_tp1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

//Classe affichage de toutes les valeurs et des taux
public class SettingsActivity extends AppCompatActivity {


    //fonction appelée a la création, récupère la liste des currency et les affichent
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        Intent myIntent = getIntent();
        ArrayList<String> listToDisp = myIntent.getStringArrayListExtra("myList");
        ListView simpleList = findViewById(R.id.maliste);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listToDisp);
        simpleList.setAdapter(arrayAdapter);


    }


}
